#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile

from conans import CMake, \
                   tools


class PkgTest_MortonND(ConanFile):
    name = "pkgtest_morton_nd"
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"

    settings = "compiler"

    requires = "morton_nd/2.0.0@bimandscan/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        pass

    def test(self):
        os.chdir("bin")
        self.run(f".{os.sep}{self.name}")
