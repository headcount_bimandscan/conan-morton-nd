#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class MortonND(ConanFile):
    name = "morton_nd"
    version = "2.0.0"
    license = "MIT"
    url = "https://bitbucket.org/headcount_bimandscan/conan-morton-nd"
    description = "A header-only compile-time Morton encoding library for N-dimensions."
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    generators = "txt"
    homepage = "https://github.com/kevinhartman/morton-nd"

    _src_dir = f"morton-nd-{version}"

    settings = "compiler"

    exports = "../LICENCE.md"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++14 or higher!")

    def source(self):
        zip_name = f"{self._src_dir}.zip"

        tools.download(f"https://github.com/kevinhartman/morton-nd/archive/v{self.version}.zip",
                       zip_name)

        tools.unzip(zip_name)
        os.unlink(zip_name)

    def build(self):
        pass

    def package(self):
        self.copy(pattern = "*.h",
                  src = f"{self._src_dir}/morton-nd/include",
                  dst = "include",
                  keep_path = False)

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.includedirs = [
                                        "include"
                                    ]

    def package_id(self):
        self.info.header_only()
