# BIM & Scan� Third-Party Library (Morton ND)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for  [Morton ND](https://github.com/kevinhartman/morton-nd), a header-only compile-time Morton encoding library for N-dimensions.

Requires a C++14 compiler (or higher).

Supports version 2.0.0 (stable).
