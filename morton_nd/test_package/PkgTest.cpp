/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <mortonND_LUT_encoder.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Morton ND' package test (compilation, linking, and execution).\n";

    // From examples:
    constexpr auto MortonND_2D_32 = mortonnd::MortonNDLutEncoder<2,
                                                                 16,
                                                                 8>();

    constexpr auto MortonND_3D_64 = mortonnd::MortonNDLutEncoder<3,
                                                                 21,
                                                                 16>();

    const auto encoding_2d = MortonND_2D_32.Encode(100,
                                                   -222);

    const auto encoding_3d = MortonND_3D_64.Encode(-235645,
                                                   1331,
                                                   56);

    std::cout << "'Morton ND' package works!" << std::endl;
    return EXIT_SUCCESS;
}
